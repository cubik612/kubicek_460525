
def start():
    print("Vítej v předaleké galaxii jménem Pythonia na planetě jménem Destiny.")
    print("V tomto světě každý dostane zadaný svůj úkol a je jen a jen na něm, jak s ním naloží.")
    print("Tvým úkolem je záchrana princezny Javy před dezinformujícími útoky lorda Pitomia.")
    print("Princezna Java je jednou z nejdůležitějších postav celé galaxie a je velice důležité, aby její mysl nebyla zmanipulována")
    print("Nyní je na tobě, jak se rozhodneš")
    print("a = Jsem věrný volič SPD do galaktického senátu a souhlasím s lordem Pitomiem.")
    print("b = Záleží mi na osudu galaxie a jsem ochoten udělat vše pro jeho světlou budoucnost!")
    volba = input()
    if volba == "a":
        konec_svobodneho_sveta()
    elif volba == "b":
        prvni_krok()

def konec_svobodneho_sveta():
    print("Celý svobodný a demokraticky smýšlející svět je zasažen, jelikož jejich vyvolený sešel z cesty....")
    print("Bohužel se s největší pravděpodobností naplní jejich noční okamůra...")
    print("Game over!")

def prvni_krok():
    print("Rozhodl jsi se správně,ale tvůj úkol nebude snadný")
    print("Nejprve musíš poznat svého nepřítele")
    print("Dostal se se nám do rukou seznam hesel, kterými se zlý lord rád ohání")
    print("Je na tobě, aby jsi vybral tu správnou kombinaci!")
    print("a = láska, pravda a demokracie")
    print("b = nelegální migrace, nacionalizmus a nenávist")
    print("c = kampaň, čapí hnízdo a zloděj Kalousek")
    volba = input()
    if volba == "a":
        achjo()
    elif volba == "b":
        druhy_krok()
    elif volba == "c":
        bures()

def achjo():
    print("Tvoje volba nebyla správná, tudíž jsi prokázal neznalost svého soupeře.")
    print("Bohužel zlého lorda nikdy nebudeš schopný přemoci")
    print("Game over")

def bures():
    print("Stalo se nečekané, ale zaměnil si lorda Pitomia a agenta Bureše")
    print("Agent Bureš je taktéž na seznamu nepřátel odboje, ale není tvým úkolem, a proto jsi zklamal")

def druhy_krok():
    print("Úspěšně jsi zvládl svůj první úkol.")
    print("Nyní tě čeká další výzva.")
    print("Budeš muset zvládnout vyvrátit jednu z lordových lží na zasedání galaktického senátu, kam se nám tě podařilo propašovat")
    print(".....")
    print(".....")
    print("Právě probíhá zasedání galaktického senátu")
    print("Lord Pitomio právě prohlásil, že za nové případy nilské horečky můžou nelegální migranti z galaxie Syrian")
    print("Nyní je tvoje šance, abys projevil odpor a nahlodal důvěru princezny v jeho slova.....co řekneš?")
    print("a = Ne! To jsou nesmyslné lži......přece mu to nemůžete věřit!!!")
    print("b = Prosím o slovo...doufám, že je všem jasné, že se tato nemoc šíří díky komárníkům..ti se sem dostali během přepravy potravin agrogiganta Bureše!!!")
    print("c = Zdravím....chtěl bych vyvrátit lordovo tvrzení, a to tím, že nilská horečka pochází z planety Nil, která neleží v galaxii Syrian")
    volba = input()
    if volba == "a":
        panika()
    elif volba == "b" or volba == "c":
        treti_krok()

def panika():
    print("Vzhledem k tvé hlasité reakci si někteří lidé uvědomili, že tě v senátu nikdy neviděli a začli konat.")
    print("Byl jsi vyveden a následně uvězněn.")
    print("Takhle budoucnoust galaxie nikdy nezachráníš")
    print("Game over")

def treti_krok():
    print("Tvoje slova se neminuly účinkem a podařilo se ti nahlodat autoritu a důvěryhodnost lorda Pitomia před celým galaktických senátem.")
    print("Dostali se k nám zprávy o tom, že po tvém činu je zlý lord přinucen k činu a chystá se implantovat princezně čip poslušnosti.")
    print("Co uděláš?")
    print("a = Dám mu BAN!")
    print("b = Uskutečním atentán.")
    volba = input()
    if volba == "a":
        yes()
    if volba == "b":
        zrada()

def zrada():
    print("Úspěšně se ti podařilo spáchat atentát, ale o smrti lorda Pitomia se rozkřiklo po celé galaxia a stal se mučedníkem")
    print("Naneštěstí má jeho odkaz mnohem více následovníků než kdy dřív a konec svobody v galaxii se blíží.")


def yes():
    print("To bylo to správné řešení")
    print("Proti zlu jsi použil jeho vlastní zbraň")
    print("Díky tomu i princezna prokoukla jeho zlé úmysly")

    print("Naplnil si svůj osud!!!")

start()
