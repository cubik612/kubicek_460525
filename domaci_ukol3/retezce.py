# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'
print((string1[:14]) + '...')
# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.

print(string1[-11:])

# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'
a, b, c, d = string2.split(', ')
print(a)
print(b)
print(c)
print(d)
# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.

def pocet_pismen(retezec, char):
    pocet = 0
    for c in retezec:
     if char == c:
      pocet += 1
    return pocet
print(pocet_pismen(string2, 'a'))

print(pocet_pismen(string2, 'b'))


# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
datetime1 = '20181121191930'
import datetime
year = int(datetime1[:4])
month = int(datetime1[4:6])
day = int(datetime1[6:8])
hour = int(datetime1[8:10])
minute = int(datetime1[10:12])
second = int(datetime1[12:])

datetime2 = datetime.datetime(year, month, day, hour, minute, second)
current_time = datetime2.today()
diff_hours = (current_time - datetime2).days * 24 + (current_time - datetime2).seconds / 3600
print(diff_hours)

# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'

wrong_order = string3.split(' ')
correct_order = [None for i in range(len(wrong_order))]
change = {}
for word in wrong_order:
    index = correct_string3.split(' ').index(word)
    correct_order[index] = word
    change[word] = abs(wrong_order.index(word) - index)

print((' ').join(correct_order))
for word, change in change.items():
    print(word + ': ' + str(change))




