### Domácí úkol 3
Tentokrát se domácí úkol bude skládat ze dvou částí. První část je zahřívací ale ke konci už se docela zapotíte! Druhá část je těžší a abstraktnější a je zaměřená na práci s dvojrozměrným polem.

Úkoly nejsou dělané čistě jen jako opakování. Cílem je abyste se naučili i něco nového, takže zadání bude obsahovat i odkazy na zdroje, které si budete muset projít.

Bodování je u každého zadání zvlášť. **V zadání se mohou vyskytovat i teoretické otázky, které se budete muset najít**.

Datum odevzdání pro první úkol bude za týden **(28.11. 23:59)**. Pro druhý úkol **(02.12. 23:59)** Odevzdávejte k sobě na gitlab. Pokud budete mít nějaké otázky k zadání nebo tam najdete nějakou chybu tak mi klidně napiště na mail.

**Budu hodnotit i neúplné a nefungující implementace. Můžete si radit a neopisujte :-(**

  